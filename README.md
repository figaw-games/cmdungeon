# CMDungeon

A text-based not-dungeon crawler, not-rpg,
dungeon-crawler/rpg-themed REPL game in python 3!

## How to play

```shell
python3 game.py
```

## Why

Wax on, wax off.. there's nothing inherently difficult in this,
or anything new and mindblowing. It's just a selfmade programming exercise,
to practice some Python.
