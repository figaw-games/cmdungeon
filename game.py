import sys
import random
import math
import os

random.seed(os.urandom(8))

parsed = ""

print("Use the 'quit' command to exit")

# http://www.network-science.de/ascii/
print("")
print("  ,- _~.   /\\\\,/\\\\, -_____")
print(" (' /|    /| || ||    ' | -,                _")
print("((  ||    || || ||   /| |  |` \\\\ \\\\ \\\\/\\\\  / \\\\  _-_   /'\\\\ \\\\/\\\\")
print("((  ||    ||=|= ||   || |==|| || || || || || || || \\\\ || || || ||")
print(" ( / |   ~|| || ||  ~|| |  |, || || || || || || ||/   || || || ||")
print("  -____-  |, \\\\,\\\\,  ~-____,  \\\\/\\\\ \\\\ \\\\ \\\\_-| \\\\,/  \\\\,/  \\\\ \\\\")
print("         _-         (                      /  \ ")
print("                                          '----`")
print("")

difficultyChosen = False

while difficultyChosen is not True:
    print("Input a number between 0 and 100 to choose difficulty,")
    print("0 = impossible, 5 = hard, 10 = normal, 100 = really?..")
    try:
        difficulty = 10

        difficultyInput = input("Choose a difficulty (normal): ").strip()

        if difficultyInput is "":
            break

        difficulty = int(difficultyInput)
    except ValueError:
        print("Input was not a number")
        print("---")
        continue
    else:

        if difficulty is None:
            break

        if difficulty < 0:
            print("Input was below 0")
            print("---")
            continue
        elif difficulty > 100:
            print("Input was above 100")
            print("---")
            continue
        break

    difficultyChosen = True

print(f'Difficulty: {difficulty}')

isDone = False
isDead = False

player_score = 0

lootModifier = 1

adjectives = [
    'silly',
    'calm',
    'agreeable',
    'eager',
    'brave',
    'ambitious',
    'delightful',
    'faithful',
    'aggressive',
    'victorious'
]

def buy(item, price, player_score, difficulty):

    # allow item to vary by 50-150%
    variation = price/2

    maxEffect = price + variation
    minEffect = price - variation

    effectiveness = random.randrange(minEffect, maxEffect)

    adjectiveIndex = math.floor((effectiveness - minEffect) / (maxEffect - minEffect) * 100 / len(adjectives))

    adjective = adjectives[adjectiveIndex]

    if player_score > price:
        print(f'You bought the {adjective} {item} for ${price}!')
        difficulty += effectiveness
        player_score -= price
    else:
        print("Not enough money!")
    return player_score, difficulty

minLoot = random.randrange(3, 7)
maxLoot = random.randrange(20, 27)

while parsed != "quit" and isDone is not True:
    cmd = input("Enter command (quest): ")
    parsed = cmd.lower().strip()

    if parsed == 'quest' or parsed == '':
        bounty = math.floor(random.randrange(minLoot, maxLoot) * lootModifier)

        if random.randrange(difficulty + 1) == 0:
            print("You die!")
            isDone = True
            isDead = True
        else:
            print(f'You quest and increase loot by ${bounty}!')
            player_score += bounty;
    elif parsed == 'help':
        print("Available commands: ")
        print("quest:  take on a perilous quest for loot!")
        print("retire: live out your days in peace with the earnings from your adventures")
        print("boast:  brag at the local tavern and take on more risky quests for a chance at more loot!")
        print("loot:   show your current earnings")
        print("shop:   visit the local merchant")
    elif parsed == 'retire':
        print("You retire!")
        isDone = True
    elif parsed == 'boast':
        print("You boast at the local tavern about all monsters you have slain")
        print(".. and surely you could take on tougher ones for a greater reward!")

        # increase loot modifier by 30-90%
        lootModifier *= 1 + (1 / 10 * random.randrange(3, 9))

        # increase difficulty by 50-80%
        difficulty = math.floor(difficulty * (1 / 10 * random.randrange(5, 8)))

    elif parsed == 'loot':
        print(f'You have ${player_score}')
    elif parsed == 'shop':
        print("Use 'buy <item>' to buy an item\nthat will help on your journey!")
        print("---")
        print("Item    Price")
        print("------------")
        print("Shield  $10")
        print("Sword   $20")
        print("Armor   $40")
    elif parsed == 'buy shield':
        player_score, difficulty = buy('shield', 10, player_score, difficulty)
    elif parsed == 'buy sword':
        player_score, difficulty = buy('sword', 20, player_score, difficulty)
    elif parsed == 'buy armor':
        player_score, difficulty = buy('armor', 40, player_score, difficulty)

    print(f'---')

print(f'Final score: {player_score}')

if isDead:
    print("What a nice funeral you will get...")
    print("When your corpse is recovered!")
